# Angular-2

## Installation

You need [Node.js](http://nodejs.org/download/). 
If you already have node.js use:

```bash
$ npm install
```

Also you need download and install my [backend api](https://bitbucket.org/BorzenkoMisha/smtp-node-server);
If you already have it use:

```bash
$ npm install
$ NODE_ENV="development" nodemon app.js
$ NODE_ENV="development" nodemon smtp.js
```
After run server, return to your`s Angular-2 folder and use: 
```bash
$ npm start
```

Also you need [smtp-client](https://bitbucket.org/BorzenkoMisha/smtp-mailer), send mail and add it to db;

```bash
$ npm install
$ node app.js
```

go to your mongo db(smtp-mail) and :

```bash
mongo.users.insert({'email':'borzenkomihail@yahoo.com', 'name': ''})
```

https://bitbucket.org/BorzenkoMisha/smtp-mailer