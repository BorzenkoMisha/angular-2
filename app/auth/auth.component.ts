// TODO redirect after login

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import * as _ from 'lodash';

const template = require('./auth.component.html');

export class User {
  email: string;
  password: string;
};

@Component({
  selector: 'auth',
  template
})

export class AuthComponent {
  constructor(private router: Router) {};
  userData: User = {
    email: 'angular@test.com',
    password: 'test1234'
  };
  user: User = {
    email: 'angular@test.com',
    password: 'test1234'
  };
  submitUser(userData:User) {
    if(_.isEqual(userData, this.user)) {
      this.router.navigateByUrl('/mailbox');
    }
  }
}
