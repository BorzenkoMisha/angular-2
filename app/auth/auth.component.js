// TODO redirect after login
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var _ = require('lodash');
var template = require('./auth.component.html');
var User = (function () {
    function User() {
    }
    return User;
}());
exports.User = User;
;
var AuthComponent = (function () {
    function AuthComponent(router) {
        this.router = router;
        this.userData = {
            email: 'angular@test.com',
            password: 'test1234'
        };
        this.user = {
            email: 'angular@test.com',
            password: 'test1234'
        };
    }
    ;
    AuthComponent.prototype.submitUser = function (userData) {
        if (_.isEqual(userData, this.user)) {
            this.router.navigateByUrl('/mailbox');
        }
    };
    AuthComponent = __decorate([
        core_1.Component({
            selector: 'auth',
            template: template
        }), 
        __metadata('design:paramtypes', [router_1.Router])
    ], AuthComponent);
    return AuthComponent;
}());
exports.AuthComponent = AuthComponent;
//# sourceMappingURL=auth.component.js.map