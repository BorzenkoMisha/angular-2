"use strict";
var router_1 = require('@angular/router');
var mailbox_component_1 = require('./mailbox/mailbox.component');
var auth_component_1 = require('./auth/auth.component');
var mails_list_component_1 = require('./mailbox/mails/mails-list/mails-list.component');
var mail_detail_component_1 = require('./mailbox/mails/mail-detail/mail-detail.component');
exports.routes = [
    { path: '', redirectTo: 'auth' },
    { path: 'auth', component: auth_component_1.AuthComponent },
    { path: 'mailbox', component: mailbox_component_1.MailboxComponent, children: [
            { path: '', redirectTo: 'mails/list' },
            { path: 'mails/list', component: mails_list_component_1.MailsListComponent },
            { path: 'mails/:id', component: mail_detail_component_1.MailDetailComponent }
        ] }
];
exports.APP_ROUTER_PROVIDERS = [
    router_1.provideRouter(exports.routes)
];
//# sourceMappingURL=app.routes.js.map