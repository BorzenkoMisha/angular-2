import { provideRouter, RouterConfig } from '@angular/router';

import { MailboxComponent } from './mailbox/mailbox.component';
import { AuthComponent } from './auth/auth.component';
import { MailsListComponent } from './mailbox/mails/mails-list/mails-list.component';
import { MailDetailComponent } from './mailbox/mails/mail-detail/mail-detail.component';

export const routes: RouterConfig = [
  { path: '', redirectTo: 'auth' },
  { path: 'auth', component: AuthComponent },
  { path: 'mailbox', component: MailboxComponent, children: [
    { path: '',  redirectTo: 'mails/list' },
    { path: 'mails/list',  component: MailsListComponent },
    { path: 'mails/:id', component: MailDetailComponent }
    ] }
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];
