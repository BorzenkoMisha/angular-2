import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { MailboxComponent } from './mailbox/mailbox.component';
import { AuthComponent } from './auth/auth.component';

const template = require('./app.component.html');

@Component({
  selector: 'my-app',
  template,
  directives: [ROUTER_DIRECTIVES]
})



export class AppComponent {

}
