import { Component } from '@angular/core';

const template = require('./header.component.html');

@Component({
  selector: 'app-header',
  template
})

export class HeaderComponent { }
