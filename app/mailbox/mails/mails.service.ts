import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import * as _ from 'lodash';

@Injectable()
export class MailsService {
  constructor (private http: Http) {}
  list () {
    return this.http.get('http://borzenko-lx:8000/mails')
                    .map(res => res.json())
  }
  detail (id:string) {
    return this.http.get(`http://borzenko-lx:8000/mails/${id}`)
                    .map(res => res.json())
  }
  update(mail:any) {
    let id = mail._id;
    mail = _.omit(mail, ['__v', '_id', 'created', 'updated']);
    console.log(mail);
    return this.http.put(`http://borzenko-lx:8000/mails/${id}`, mail)
                    .map(res => res.json());
  }

}
