"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var FormattedDataComponent = (function () {
    function FormattedDataComponent() {
        this.newDate = this.date;
    }
    FormattedDataComponent.prototype.ngOnInit = function () {
        this.newDate = new Date(this.date).toDateString();
        console.log('This if the value for user-id: ' + this.newDate);
    };
    FormattedDataComponent.prototype.testFunction = function (date) {
        console.log(date);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], FormattedDataComponent.prototype, "date", void 0);
    FormattedDataComponent = __decorate([
        core_1.Component({
            selector: 'formatted-data',
            template: "{{newDate}}"
        }), 
        __metadata('design:paramtypes', [])
    ], FormattedDataComponent);
    return FormattedDataComponent;
}());
exports.FormattedDataComponent = FormattedDataComponent;
//# sourceMappingURL=formatted-data.component.js.map