import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { MailsService } from '../mails.service';
import { FormattedDataComponent } from '../formatted-data.component';
const template = require('./mails-list.component.html');

@Component({
  selector: 'mails-list',
  template,
  directives: [FormattedDataComponent, ROUTER_DIRECTIVES],
  providers: [MailsService]
})

export class MailsListComponent {
  mails:any;
  count:number;
  constructor(private MailsService: MailsService,  private router: Router) { }

  list() {
    this.MailsService.list()
      .subscribe(
      (mails:any) => {
        this.count = mails.length;
        this.mails = mails
       });
  }

  ngOnInit() {
    this.list();
  }

}
