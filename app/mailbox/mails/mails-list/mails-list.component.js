"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var mails_service_1 = require('../mails.service');
var formatted_data_component_1 = require('../formatted-data.component');
var template = require('./mails-list.component.html');
var MailsListComponent = (function () {
    function MailsListComponent(MailsService, router) {
        this.MailsService = MailsService;
        this.router = router;
    }
    MailsListComponent.prototype.list = function () {
        var _this = this;
        this.MailsService.list()
            .subscribe(function (mails) {
            _this.count = mails.length;
            _this.mails = mails;
        });
    };
    MailsListComponent.prototype.ngOnInit = function () {
        this.list();
    };
    MailsListComponent = __decorate([
        core_1.Component({
            selector: 'mails-list',
            template: template,
            directives: [formatted_data_component_1.FormattedDataComponent, router_1.ROUTER_DIRECTIVES],
            providers: [mails_service_1.MailsService]
        }), 
        __metadata('design:paramtypes', [mails_service_1.MailsService, router_1.Router])
    ], MailsListComponent);
    return MailsListComponent;
}());
exports.MailsListComponent = MailsListComponent;
//# sourceMappingURL=mails-list.component.js.map