import { Component, Input } from '@angular/core';

@Component({
  selector: 'formatted-data',
  template: `{{newDate}}`
})

export class FormattedDataComponent {
  @Input() date:string;
  newDate:string;

  ngOnInit() {
    this.newDate = new Date(this.date).toDateString(); 
    console.log('This if the value for user-id: ' + this.newDate);
  }

  constructor() {
    this.newDate = this.date;
  }

  testFunction(date:string) {
    console.log(date);
  }
}
