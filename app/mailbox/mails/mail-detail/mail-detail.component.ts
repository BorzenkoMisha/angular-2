import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MailsService } from '../mails.service';
import { FormattedDataComponent } from '../formatted-data.component';
const template = require('./mail-detail.component.html');

@Component({
  selector: 'mail-detail',
  template,
  directives: [FormattedDataComponent],
  providers: [MailsService]
})

export class MailDetailComponent {
  mail: any;
  private sub: any;
  isDataAvailable:boolean = false;
  constructor(private MailsService: MailsService, private route: ActivatedRoute){}

  detail(id:string) {
    this.MailsService.detail(id)
      .subscribe(
      (mail:any) => {
        this.mail = mail;
        if(!this.mail.isRead) {
          this.mail.isRead = true;
          this.update(this.mail);
        }
        this.isDataAvailable = true;
       });
  }

  update(mail:any) {
    this.MailsService.update(mail)
      .subscribe(
      (mail:any) => {
        console.log(mail);
       });
  }

  ngOnInit(){
    this.sub = this.route
      .params
      .subscribe( params => {
        let id = params['id'];
        this.detail(id);
      });
  }
}
