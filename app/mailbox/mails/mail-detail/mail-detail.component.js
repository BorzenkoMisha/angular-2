"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var mails_service_1 = require('../mails.service');
var formatted_data_component_1 = require('../formatted-data.component');
var template = require('./mail-detail.component.html');
var MailDetailComponent = (function () {
    function MailDetailComponent(MailsService, route) {
        this.MailsService = MailsService;
        this.route = route;
        this.isDataAvailable = false;
    }
    MailDetailComponent.prototype.detail = function (id) {
        var _this = this;
        this.MailsService.detail(id)
            .subscribe(function (mail) {
            _this.mail = mail;
            if (!_this.mail.isRead) {
                _this.mail.isRead = true;
                _this.update(_this.mail);
            }
            _this.isDataAvailable = true;
        });
    };
    MailDetailComponent.prototype.update = function (mail) {
        this.MailsService.update(mail)
            .subscribe(function (mail) {
            console.log(mail);
        });
    };
    MailDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route
            .params
            .subscribe(function (params) {
            var id = params['id'];
            _this.detail(id);
        });
    };
    MailDetailComponent = __decorate([
        core_1.Component({
            selector: 'mail-detail',
            template: template,
            directives: [formatted_data_component_1.FormattedDataComponent],
            providers: [mails_service_1.MailsService]
        }), 
        __metadata('design:paramtypes', [mails_service_1.MailsService, router_1.ActivatedRoute])
    ], MailDetailComponent);
    return MailDetailComponent;
}());
exports.MailDetailComponent = MailDetailComponent;
//# sourceMappingURL=mail-detail.component.js.map