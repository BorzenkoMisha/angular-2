"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var label_service_1 = require('./label.service');
var LabelComponent = (function () {
    function LabelComponent(LabelService) {
        this.LabelService = LabelService;
    }
    LabelComponent.prototype.list = function () {
        var _this = this;
        this.LabelService.list().then(function (labels) { return _this.labels = labels; });
    };
    LabelComponent.prototype.ngOnInit = function () {
        this.list();
    };
    LabelComponent = __decorate([
        core_1.Component({
            selector: 'labels',
            templateUrl: 'app/mailbox/sidebar/label/label.component.html',
            providers: [label_service_1.LabelService]
        }), 
        __metadata('design:paramtypes', [label_service_1.LabelService])
    ], LabelComponent);
    return LabelComponent;
}());
exports.LabelComponent = LabelComponent;
;
//# sourceMappingURL=label.component.js.map