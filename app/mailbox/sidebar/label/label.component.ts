import { Component, OnInit } from '@angular/core';
import { LabelService } from './label.service';
import { Label } from './label';


@Component({
  selector: 'labels',
  templateUrl: 'app/mailbox/sidebar/label/label.component.html',
  providers: [LabelService]
})

export class LabelComponent implements OnInit {
  labels: Label[];
  constructor(private LabelService: LabelService) { }

  list() {
    this.LabelService.list().then(labels => this.labels = labels);
  }

  ngOnInit() {
    this.list();
  }

};
