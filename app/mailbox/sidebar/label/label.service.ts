import { Injectable } from '@angular/core';
import { Label } from './label';

const LABELS: Label[] = [
  { title: 'Family' },
  { title: 'Work' },
  { title: 'Home' },
  { title: 'Children' },
  { title: 'Holidays' },
  { title: 'Music' },
  { title: 'Photography' },
  { title: 'Film' }
];

@Injectable()
export class LabelService {
  list() {
    return Promise.resolve(LABELS);
  }
}
