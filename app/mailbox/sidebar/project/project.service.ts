import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Project } from './project';
import * as _ from 'lodash';

@Injectable()
export class ProjectService {
  constructor (private http: Http) {}
  list () {
    return this.http.get('http://borzenko-lx:8000/projects')
                    .map(res => res.json())
  }
  remove(project:Project) {
    return this.http.delete(`http://borzenko-lx:8000/projects/${project._id}`)
                    .map(res => res.json());
  }
  save(project:Project) {
    if (project._id) {
      return this.update(project);
    }
    return this.create(project);
  }
  update(project:Project) {
    let updatingProject =  { 'title': project.title };
    return this.http.put(`http://borzenko-lx:8000/projects/${project._id}`, updatingProject)
                    .map(res => res.json());
  }
  create(project:any) {
    return this.http.post(`http://borzenko-lx:8000/projects/`, project)
                    .map(res => res.json());
  }
}
