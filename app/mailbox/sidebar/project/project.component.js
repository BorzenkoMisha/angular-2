"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var project_service_1 = require('./project.service');
var template = require('./project.component.html');
var ProjectComponent = (function () {
    function ProjectComponent(ProjectService) {
        this.ProjectService = ProjectService;
        this.newProject = {
            title: ''
        };
    }
    ProjectComponent.prototype.list = function () {
        var _this = this;
        this.ProjectService.list()
            .subscribe(function (projects) { return _this.projects = projects; });
    };
    ProjectComponent.prototype.update = function (project) {
        if (project.isEditting === false)
            return project.isEditting = !project.isEditting;
        this.ProjectService.save(project)
            .subscribe(function (res) { return project.isEditting = !project.isEditting; });
    };
    ProjectComponent.prototype.create = function (project) {
        var _this = this;
        this.ProjectService.save(project)
            .subscribe(function (project) {
            _this.projects.push(project);
            _this.newProject = { title: '' };
        });
    };
    ProjectComponent.prototype.remove = function (project) {
        var _this = this;
        this.ProjectService.remove(project)
            .subscribe(function () { return _this.projects.splice(_this.projects.indexOf(project), 1); });
    };
    ProjectComponent.prototype.ngOnInit = function () {
        this.list();
    };
    ProjectComponent = __decorate([
        core_1.Component({
            selector: 'projects',
            template: template,
            providers: [project_service_1.ProjectService]
        }), 
        __metadata('design:paramtypes', [project_service_1.ProjectService])
    ], ProjectComponent);
    return ProjectComponent;
}());
exports.ProjectComponent = ProjectComponent;
;
//# sourceMappingURL=project.component.js.map