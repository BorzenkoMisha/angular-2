import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/common';
import { ProjectService } from './project.service';
import { Project } from './project';

const template = require('./project.component.html');

@Component({
  selector: 'projects',
  template,
  providers: [ProjectService]
})

export class ProjectComponent implements OnInit {
  projects:Project[];
  newProject = {
    title: ''
  };
  constructor(private ProjectService: ProjectService) { }

  list() {
    this.ProjectService.list()
      .subscribe(
        projects => this.projects = projects);
  }

  update(project:Project) {
    if (project.isEditting === false) return project.isEditting = !project.isEditting;
    this.ProjectService.save(project)
      .subscribe(
        res => project.isEditting = !project.isEditting );
  }

  create(project:any) {
    this.ProjectService.save(project)
      .subscribe(
        (project:Project) => {
          this.projects.push(project);
          this.newProject = { title:'' };
        });
  }

  remove(project:Project) {
    this.ProjectService.remove(project)
      .subscribe(() => this.projects.splice(this.projects.indexOf(project), 1) );
  }

  ngOnInit() {
    this.list();
  }

};
