class User {
  email: string;
  name: string;
}
export class Project {
  _id: string;
  __v: string;
  title: string;
  users: User;
  created: string;
  updated: string;
  isEditting: boolean;
}
