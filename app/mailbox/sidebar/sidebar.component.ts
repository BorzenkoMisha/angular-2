import { Component, OnInit } from '@angular/core';
import { ProjectComponent } from './project/project.component';
import { FolderComponent } from './folder/folder.component';
import { LabelComponent } from './label/label.component';

const template = require('./sidebar.component.html');

@Component({
  selector: 'sidebar',
  template,
  directives: [ProjectComponent, FolderComponent, LabelComponent]
})

export class SidebarComponent { };
