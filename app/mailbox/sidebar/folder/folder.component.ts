import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { FolderService } from './folder.service';
import { Folder } from './folder';


@Component({
  selector: 'folders',
  templateUrl: 'app/mailbox/sidebar/folder/folder.component.html',
  directives: [ROUTER_DIRECTIVES],
  providers: [FolderService]
})

export class FolderComponent implements OnInit {
  folders: Folder[];
  constructor(private FolderService: FolderService) { }

  list() {
    this.FolderService.list().then(folders => this.folders = folders);
  }

  ngOnInit() {
    this.list();
  }

};
