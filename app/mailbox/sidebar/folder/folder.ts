export class Folder {
  title: string;
  iconClass: string;
  unread: number;
  link: string;
};
