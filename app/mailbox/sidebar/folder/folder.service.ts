import { Injectable } from '@angular/core';
import { Folder } from './folder';

const FOLDERS: Folder[] = [
  { title: 'Inbox', iconClass: 'fa-inbox', unread: 2, link: 'mails/list' },
  { title: 'Send Mail', iconClass: 'fa-envelope-o', unread: 3, link: 'mails/list' },
  { title: 'Important', iconClass: 'fa-certificate', unread: 2, link: 'mails/list' },
  { title: 'Draft', iconClass: 'fa-file-text-o', unread: 0, link: 'mails/list' },
  { title: 'Trash', iconClass: 'fa-trash-o', unread: 5, link: 'mails/list' }
];

@Injectable()
export class FolderService {
  list() {
    return Promise.resolve(FOLDERS);
  }
}
