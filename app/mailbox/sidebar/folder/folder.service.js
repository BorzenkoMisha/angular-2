"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var FOLDERS = [
    { title: 'Inbox', iconClass: 'fa-inbox', unread: 2, link: 'mails/list' },
    { title: 'Send Mail', iconClass: 'fa-envelope-o', unread: 3, link: 'mails/list' },
    { title: 'Important', iconClass: 'fa-certificate', unread: 2, link: 'mails/list' },
    { title: 'Draft', iconClass: 'fa-file-text-o', unread: 0, link: 'mails/list' },
    { title: 'Trash', iconClass: 'fa-trash-o', unread: 5, link: 'mails/list' }
];
var FolderService = (function () {
    function FolderService() {
    }
    FolderService.prototype.list = function () {
        return Promise.resolve(FOLDERS);
    };
    FolderService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], FolderService);
    return FolderService;
}());
exports.FolderService = FolderService;
//# sourceMappingURL=folder.service.js.map