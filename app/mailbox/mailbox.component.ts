import { Component } from '@angular/core';
import { Router, ROUTER_DIRECTIVES } from '@angular/router';

import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';

const template = require('./mailbox.component.html');

@Component({
  selector: 'mailbox',
  template,
  directives: [HeaderComponent, SidebarComponent, ROUTER_DIRECTIVES]
})


export class MailboxComponent { }
