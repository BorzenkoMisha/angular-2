"use strict";
var router_1 = require('@angular/router');
var mailbox_component_1 = require('./mailbox/mailbox.component');
var auth_component_1 = require('./auth/auth.component');
exports.routes = [
    { path: '/', component: auth_component_1.AuthComponent },
    { path: '/auth', component: auth_component_1.AuthComponent },
    { path: '/mailbox', component: mailbox_component_1.MailboxComponent }
];
exports.APP_ROUTER_PROVIDERS = [
    router_1.provideRouter(exports.routes)
];
//# sourceMappingURL=routes.js.map